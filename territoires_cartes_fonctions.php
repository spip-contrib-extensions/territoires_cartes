<?php
/**
 * Fonctions utiles au plugin Cartes de Territoires
 *
 * @plugin     Cartes de territoires
 * @copyright  2021
 * @author     Eric Lupinacci
 * @licence    GNU/GPL
 * @package    SPIP\Cartes_territoires\Fonctions
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * Compile la balise `#TERRITOIRE_CARTE_INFOS` qui renvoie la description augmentée d'une carte connue par son id.
 * La signature de la balise est : `#TERRITOIRE_CARTE_INFOS{id_carte}`.
 *
 * @balise
 *
 * @uses territoire_carte_informer()
 *
 * @param Champ $p Pile au niveau de la balise.
 *
 * @return Champ Pile complétée par le code à générer.
 **/
function balise_TERRITOIRE_CARTE_INFOS_dist(Champ $p) : Champ {
	// Récupération des arguments.
	// -- la balise utilise toujours le rangement par rang au sein du conteneur
	// -- et ne permet pas de filtrer les noisettes autrement que sur le conteneur.
	$id_carte = interprete_argument_balise(1, $p);
	$id_carte = isset($id_carte) ? str_replace('\'', '"', $id_carte) : '""';

	// On appelle la fonction de calcul de la liste des noisettes
	$p->code = "(include_spip('inc/territoire_carte')
		? territoire_carte_informer({$id_carte})
		: [])";

	return $p;
}
