<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Edition d'une carte de territoires utilisée uniquement pour la modification.
 *
 * @param null|string $arguments Arguments de l'action limités au seul id de la carte
 *
 * @return array Id et message d'erreur si besoin.
 */
function action_editer_territoire_carte_dist(string $arguments = null) {
	if (null === $arguments) {
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$arguments = $securiser_action();
	}

	// Cette fonction n'est appelée que pour une modification de la carte, jamais pour une création.
	$erreur = '';
	if ($id_carte = (int) $arguments) {
		// Récupération des champs du formulaire d'édition
		$set = [
			'titre'      => _request('titre') ?: '',
			'descriptif' => _request('descriptif') ?: '',
		];

		// Collecter le mode et le crible final
		$mode_crible = _request('mode_crible');
		$crible = _request('crible') ?? [];
		// -- si inclusion et crible vide, on force le mode à exclusion pour avoir un contenu
		if (
			($mode_crible === 'inclusions')
			&& !$crible
		) {
			$mode_crible = 'exclusions';
		}

		// Constitution de la nouvelle valeur des paramètres
		// -- on récupère la description complète de la carte
		include_spip('action/editer_objet');
		$carte = objet_lire('territoire_carte', $id_carte);
		$parametres = json_decode($carte['parametres'], true);
		// -- on supprime le crible éventuel
		unset($parametres['exclusions'], $parametres['inclusions']);
		// On met le nouveau
		$parametres[$mode_crible] = $crible;

		// Mise à jour de la carte
		$set['parametres'] = json_encode($parametres);
		objet_modifier('territoire_carte', $id_carte, $set);
	}

	return [$id_carte, $erreur];
}
