<?php
/**
 * Action supprimer pour l'objet territoire_carte.
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Action pour supprimer une territoire_carte.
 *
 * @param null|int $arg Identifiant à supprimer.
 *                      En absence de l'id l'action utilise l'argument sécurisée.
 *
 * @return void
**/
function action_supprimer_territoire_carte_dist(?int $arg = null) : void {
	$need_confirm = false;
	if (null === $arg) {
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$arg = $securiser_action();
		$need_confirm = true;
	}
	$id_carte = (int) $arg;

	if ($need_confirm) {
		confirmer_supprimer_territoire_carte_avant_action(
			_T('territoire_carte:confirmer_supprimer_territoire_carte'),
			_T('item_oui') . '! ' . _T('territoire_carte:supprimer_territoire_carte')
		);
	}

	// cas suppression
	if (autoriser('supprimer', 'territoire_carte', $id_carte)) {
		if ($id_carte) {
			$objet = sql_fetsel('*', 'spip_territoires_cartes', 'id_carte=' . sql_quote($id_carte));
			$qui = (!empty($GLOBALS['visiteur_session']['id_auteur']) ? 'auteur #' . $GLOBALS['visiteur_session']['id_auteur'] : 'IP ' . $GLOBALS['ip']);
			spip_log("SUPPRESSION territoire_carte#{$id_carte} par {$qui} : " . json_encode($objet), 'suppressions' . _LOG_INFO_IMPORTANTE);

			sql_delete('spip_territoires_cartes', 'id_carte=' . sql_quote($id_carte));

			// invalider le cache
			include_spip('inc/invalideur');
			suivre_invalideur("id='territoire_carte/{$id_carte}'");
		} else {
			spip_log("action_supprimer_territoire_carte_dist {$id_carte} pas compris");
		}
	}
}

/**
 * Confirmer avant suppression si on arrive par un bouton action.
 *
 * @param string      $titre
 * @param string      $titre_bouton
 * @param null|string $url_action
 *
 * @return bool
 */
function confirmer_supprimer_territoire_carte_avant_action(string $titre, string $titre_bouton, ?string $url_action = null) {
	if (!$url_action) {
		$url_action = self();
		$action = _request('action');
		$url_action = parametre_url($url_action, 'action', $action, '&');
	} else {
		$action = parametre_url($url_action, 'action');
	}
	$id_carte = parametre_url($url_action, 'arg');
	$confirm = md5("{$action}:{$id_carte}:" . realpath(__FILE__));
	if (_request('confirm_action') === $confirm) {
		return true;
	}

	$url_confirm = parametre_url($url_action, 'confirm_action', $confirm, '&');
	include_spip('inc/filtres');
	$bouton_action = bouton_action($titre_bouton, $url_confirm);
	$corps = "<div style='text-align:center;'>{$bouton_action}</div>";

	include_spip('inc/minipres');
	echo minipres($titre, $corps);
	exit;
}
