<?php
/**
 * Fichier gérant l'installation et désinstallation du plugin Cartes de Territoires.
 *
 * @package    SPIP\TERRITOIRES_CARTES\BASE
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Fonction d'installation et de mise à jour du plugin Cartes.
 *
 * @param string $nom_meta_base_version Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @param string $version_cible         Version du schéma de données dans ce plugin (déclaré dans paquet.xml)
 *
 * @return void
**/
function territoires_cartes_upgrade($nom_meta_base_version, $version_cible) {
	$maj = [];

	$maj['create'] = [
		['maj_tables', ['spip_territoires_cartes', 'spip_territoires_cartes_liens']],
		['territoires_cartes_configurer']
	];

	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}

/**
 * Fonction de désinstallation du plugin Cartes.
 *
 * @param string $nom_meta_base_version Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 *
 * @return void
**/
function territoires_cartes_vider_tables($nom_meta_base_version) {
	// Supprimer les tables ajoutées par le plugin
	sql_drop_table('spip_territoires_cartes');
	sql_drop_table('spip_territoires_cartes_liens');

	// Nettoyer les liens courants (le génie optimiser_base_disparus se chargera de nettoyer toutes les tables de liens)
	sql_delete('spip_documents_liens', sql_in('objet', ['carte']));
	sql_delete('spip_mots_liens', sql_in('objet', ['carte']));
	sql_delete('spip_auteurs_liens', sql_in('objet', ['carte']));
	// Nettoyer les versionnages et forums
	sql_delete('spip_versions', sql_in('objet', ['carte']));
	sql_delete('spip_versions_fragments', sql_in('objet', ['carte']));
	sql_delete('spip_forum', sql_in('objet', ['carte']));

	// Effacer les meta du plugin
	// - la meta de configuration technique du plugin
	effacer_meta('territoires_cartes');
	// - la meta de schéma
	effacer_meta($nom_meta_base_version);
}

function territoires_cartes_configurer() {
	// Initialisation de la configuration
	// -- Configuration statique du plugin
	$config_statique = [
		'filtres' => [
			'profondeur' => [
				'types_compatibles' => ['subdivision', 'infrasubdivision'],
				'champ_id'          => 'profondeur_type',
				'champ_nom'         => '',
				'variable_liste'    => '_profondeurs',
				'variable_choix'    => 'profondeurs'
			],
			'categorie' => [
				'types_compatibles' => ['zone', 'subdivision', 'protected_area', 'infrasubdivision'],
				'champ_id'          => 'categorie',
				'champ_nom'         => '',
				'variable_liste'    => '_categories',
				'variable_choix'    => 'categories'
			],
			'parent' => [
				'types_compatibles' => ['country'],
				'champ_id'          => 'iso_parent',
				'champ_nom'         => 'nom_parent',
				'variable_liste'    => '_parents',
				'variable_choix'    => 'parents'
			],
			'parent_0' => [
				'types_compatibles' => ['subdivision', 'infrasubdivision'],
				'champ_id'          => 'iso_parent',
				'champ_nom'         => 'nom_usage',
				'variable_liste'    => '_parents_0',
				'variable_choix'    => 'parents_0'
			],
			'parent_1' => [
				'types_compatibles' => ['subdivision', 'infrasubdivision'],
				'champ_id'          => 'iso_parent',
				'champ_nom'         => 'nom_usage',
				'variable_liste'    => '_parents_1',
				'variable_choix'    => 'parents_1'
			],
			'parent_2' => [
				'types_compatibles' => ['subdivision', 'infrasubdivision'],
				'champ_id'          => 'iso_parent',
				'champ_nom'         => 'nom_usage',
				'variable_liste'    => '_parents_2',
				'variable_choix'    => 'parents_2'
			],
		],
	];

	// Ecriture de la meta associée
	ecrire_config('territoires_cartes', $config_statique);
}
