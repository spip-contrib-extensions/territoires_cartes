<?php
/**
 * Déclarations relatives à la base de données.
 *
 * @package SPIP\TERRITOIRES_CARTES\BASE
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Déclaration des alias de tables et filtres automatiques de champs.
 *
 * @pipeline declarer_tables_interfaces
 *
 * @param array $interfaces Déclarations d'interface pour le compilateur
 *
 * @return array Déclarations d'interface pour le compilateur
 */
function territoires_cartes_declarer_tables_interfaces($interfaces) {
	// Les tables : permet d'appeler une boucle avec le *type* de la table uniquement
	$interfaces['table_des_tables']['territoires_cartes'] = 'territoires_cartes';

	// Les traitements
	// - table spip_noizetier_pages : on desérialise les tableaux et on traite la typo ou les raccourcis
	$interfaces['table_des_traitements']['PARAMETRES']['territoires_cartes'] = 'json_decode(%s, true)';
	$interfaces['table_des_traitements']['TITRE']['territoires_cartes'] = _TRAITEMENT_TYPO;
	$interfaces['table_des_traitements']['DESCRIPTIF']['territoires_cartes'] = _TRAITEMENT_RACCOURCIS;

	return $interfaces;
}

/**
 * Déclaration des objets éditoriaux.
 *
 * @pipeline declarer_tables_objets_sql
 *
 * @param array $tables Description des tables
 *
 * @return array Description complétée des tables
 */
function territoires_cartes_declarer_tables_objets_sql($tables) {
	$tables['spip_territoires_cartes'] = [
		'type'       => 'territoire_carte',
		'principale' => 'oui',
		'field'      => [
			'id_carte'        => 'bigint(21) NOT NULL',
			'titre'           => 'text NOT NULL DEFAULT ""',
			'descriptif'      => 'longtext NOT NULL DEFAULT ""',
			'type_territoire' => 'varchar(20) DEFAULT "" NOT NULL',
			'type_carte'      => 'varchar(25) DEFAULT "territoire" NOT NULL',
			'iso_pays'        => 'varchar(2) NOT NULL DEFAULT ""',
			'parametres'      => 'text DEFAULT "" NOT NULL',
			'maj'             => 'timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'
		],
		'key' => [
			'PRIMARY KEY'         => 'id_carte',
			'KEY type_territoire' => 'type_territoire',
			'KEY type_carte'      => 'type_carte',
			'KEY iso_pays'        => 'iso_pays',
		],
		'titre' => 'titre AS titre, "" AS lang',
		//'date' => '',
		'champs_editables'  => ['titre', 'descriptif', 'parametres'],
		'champs_versionnes' => ['titre', 'descriptif', 'parametres'],
		'rechercher_champs' => ['titre' => 10, 'descriptif' => 5, 'type_territoire' => 2],
		'tables_jointures'  => ['spip_territoires_cartes_liens'],

		'texte_retour'     => 'icone_retour',
		'texte_objets'     => 'territoire_carte:titre_territoire_cartes',
		'texte_objet'      => 'territoire_carte:titre_territoire_carte',
		'texte_modifier'   => 'territoire_carte:icone_modifier_territoire_carte',
		'texte_creer'      => 'territoire_carte:icone_creer_territoire_carte',
		'info_aucun_objet' => 'territoire_carte:info_aucun_territoire_carte',
		'info_1_objet'     => 'territoire_carte:info_1_territoire_carte',
		'info_nb_objets'   => 'territoire_carte:info_nb_territoire_cartes',
	];

	return $tables;
}

/**
 * Déclaration des tables secondaires (liaisons).
 *
 * @pipeline declarer_tables_auxiliaires
 *
 * @param array $tables Description des tables
 *
 * @return array Description complétée des tables
 */
function territoires_cartes_declarer_tables_auxiliaires($tables) {
	$tables['spip_territoires_cartes_liens'] = [
		'field' => [
			'id_carte' => 'bigint(21) DEFAULT "0" NOT NULL',
			'id_objet' => 'bigint(21) DEFAULT "0" NOT NULL',
			'objet'    => 'varchar(25) DEFAULT "" NOT NULL',
			'role'     => 'varchar(25) DEFAULT ""',
			'vu'       => 'varchar(6) DEFAULT "non" NOT NULL',
		],
		'key' => [
			'PRIMARY KEY'  => 'id_carte,id_objet,objet,role',
			'KEY id_carte' => 'id_carte',
		]
	];

	return $tables;
}
