<?php
/**
 * Gestion du formulaire (assistant) de création d'une carte de territoires.
 *
 * @package SPIP\CARTES\OBJET
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Chargement des données :.
 *
 * @return array Tableau des données à charger par le formulaire dans l'étape 1.
 */
function formulaires_creer_territoire_carte_charger() : array {
	// Paramètres de saisie de l'étape 1 à 3:
	// -- Etape 1
	$valeurs = [
		'type'       => _request('type') ?? '',
		'type_carte' => _request('type_carte') ?? '',
	];
	include_spip('inc/config');
	$types_territoire = lire_config('territoires/types');
	foreach ($types_territoire as $_type) {
		if (lire_config("territoires/{$_type}/populated_by_country", false)) {
			$variable_pays = 'pays_' . $_type;
			$valeurs[$variable_pays] = _request($variable_pays) ?? '';
		}
	}

	// Initialisation des paramètres d'affichage de l'étape 1
	// -- Liste des types de territoire.
	foreach ($types_territoire as $_type) {
		$valeurs['_types_territoire'][$_type] = spip_ucfirst(_T("territoire:type_{$_type}"));
	}
	$valeurs['_type_territoire_defaut'] = 'country';

	// -- Types de carte et défaut.
	$valeurs['_types_carte'] = [
		'territoire' => _T('territoire_carte:type_carte_territoire'),
		'carte'      => _T('territoire_carte:type_carte_carte')
	];
	$valeurs['_type_carte_defaut'] = 'territoire';

	// -- Détermination des pays disponibles pour les types subdivision, infrasubdivision et protected_area
	//    et structuration des colonnes du choix radio.
	$select = ['t1.iso_pays', 't2.nom_usage'];
	$from = ['spip_territoires AS t1', 'spip_territoires AS t2'];
	$where = ['t2.iso_territoire=t1.iso_pays'];
	foreach ($types_territoire as $_type) {
		if (lire_config("territoires/{$_type}/populated_by_country", false)) {
			$where[1] = 't1.type=' . sql_quote($_type);
			$pays = sql_allfetsel($select, $from, $where, 't1.iso_pays');
			$valeurs['_pays'][$_type] = array_column($pays, 'nom_usage', 'iso_pays');
			$valeurs['_choix_multi_col'][$_type] = 1 + intdiv(count($valeurs['_pays'][$_type]), 10);
			if ($valeurs['_choix_multi_col'][$_type] > 3) {
				$valeurs['_choix_multi_col'][$_type] = 3;
			}
			$valeurs['_classe_conteneur'][$_type] = ($valeurs['_choix_multi_col'][$_type] > 1)
				? 'pleine_largeur'
				: '';
		}
	}

	// Initialisation des paramètres d'affichage utilisés en étape 2 et 3
	// -- Etape 2 (saisie 1)
	$valeurs['filtre_carte'] = _request('filtre_carte');
	$filtres = lire_config('territoires_cartes/filtres', []);
	foreach ($filtres as $_parametres) {
		$variable = $_parametres['variable_choix'];
		$valeurs[$variable] = _request($variable);
	}
	$valeurs['cartes'] = _request('cartes');
	// -- Etape 2 (vérification 1)
	$valeurs['_cartes_disponibles'] = _request('_cartes_disponibles');
	$valeurs['_filtres'] = _request('_filtres');
	foreach ($filtres as $_parametres) {
		$variable = $_parametres['variable_liste'];
		$valeurs[$variable] = _request($variable);
	}

	// -- Etape 3 (vérification 2)
	$valeurs['_cartes'] = _request('_cartes');
	$valeurs['_explication_filtre'] = _request('_explication_filtre');
	$valeurs['mode_crible'] = _request('mode_crible');
	$valeurs['_territoires_crible'] = _request('_territoires_crible');

	// Préciser le nombre d'étapes du formulaire
	$valeurs['_etapes'] = 3;

	return $valeurs;
}

/**
 * Vérification de l'étape 1 du formulaire :.
 *
 * @return array Message d'erreur si aucun pays choisi alors que la configuration du type de teritoire l'oblige.
 *               Sinon, chargement des champs utiles à l'étape 2 :
 */
function formulaires_creer_territoire_carte_verifier_1() : array {
	// Initialisation des erreurs de vérification.
	$erreurs = [];

	// Il est inutile de vérifier à nouveau les valeurs de l'étape 1 si on est dans une étape ultérieure
	// car on ne modifie rien. On gagne du temps en passant outre.
	include_spip('inc/config');
	if (_request('_etape') >= 1) {
		// On collecte donc les paramètres de l'étape 1.
		$type_carte = _request('type_carte');
		$type = _request('type');
		$variable_pays = 'pays_' . $type;
		$pays = _request($variable_pays) ?? '';

		if ($type_carte === 'carte') {
			// On recherche si il existe des cartes du même type qui pourrait être incluse dans
			// la carte en cours de création
			include_spip('inc/territoire_carte');
			$cartes = territoire_carte_repertorier($type);
			if ($cartes) {
				$cartes = array_column($cartes, 'titre', 'id_carte');
				set_request('_cartes_disponibles', $cartes);
			} else {
				$erreurs['message_erreur'] = _T('territoires_cartes:erreur_type_territoire_0_carte');
			}
		} elseif (
			lire_config("territoires/{$type}/populated_by_country", false)
			and !$pays
		) {
			$erreurs[$variable_pays] = _T('info_obligatoire');
		} else {
			// Le but de l'étape 2 est de filtrer l'unité de peuplement des territoires choisie en étape 1
			// par niveau, par parenté ou par catégorie suivant le type de territoires.
			// -- on initialise les listes correspondantes même si pour certains types elles resteront vides
			$listes = [];

			// -- on lit la configuration des filtres : catégorie, profondeur et parent (du type hiérarchiquement supérieur)
			$filtres = lire_config('territoires_cartes/filtres', []);

			// -- on adapte la recherche à la configuration du type
			$criteres_autorises = [];
			$from = ['spip_territoires AS t1'];
			$select = [
				't1.profondeur_type'
			];
			$where = [
				't1.type=' . sql_quote($type),
			];
			foreach ($filtres as $_critere => $_parametres) {
				// Initialisation de la liste de chaque critère indépendamment de son utilisation
				$listes[$_critere] = [];
				if (in_array($type, $_parametres['types_compatibles'])) {
					// Mémorisation de l'utilisation du critère : on construit aussi une liste affichable
					$criteres_autorises[$_critere] = _T('territoires_cartes:label_filtre_' . $_critere);
					// Calcul des conditions de la requête SQL
					$select[] = 't1.' . $_parametres['champ_id'];
					if (strpos($_critere, 'parent_') !== false) {
						$select[] = 't1.nom_usage';
						$select[] = 't1.iso_territoire';
					}
					if ($_critere === 'parent') {
						// Le critère parent est toujours le seul et dans ce cas on ne cherche que les territoires parent
						$select[] = 't2.nom_usage as nom_parent';
						$from[] = 'spip_territoires AS t2';
						$where[] = 't2.iso_territoire=t1.iso_parent';
					}
				}
			}
			// -- on s'assure que les select ne sont pas dupliqués
			$select = array_unique($select);

			// -- on ajout le pays si besoin
			if ($pays) {
				$where[] = 't1.iso_pays=' . sql_quote($pays);
			}

			// -- on récupère les territoires et on construit les listes
			$territoires = sql_allfetsel($select, $from, $where, '', 't1.profondeur_type');

			// -- on raffine les critères autorisés vérifiant que les parent_p sont possibles en fonction du max de profondeur
			$max_profondeur = max(array_column($territoires, 'profondeur_type'));
			foreach (array_keys($criteres_autorises) as $_critere) {
				if (strpos($_critere, 'parent_') !== false) {
					$profondeur = (int) str_replace('parent_', '', $_critere);
					if ($profondeur >= $max_profondeur) {
						unset($criteres_autorises[$_critere]);
					}
				}
			}

			// -- on construit les listes
			foreach ($territoires as $_territoire) {
				foreach (array_keys($criteres_autorises) as $_critere) {
					$champ = strpos($_critere, 'parent_') === false
						? $filtres[$_critere]['champ_id']
						: 'iso_territoire';
					$champ_nom = $filtres[$_critere]['champ_nom'];
					$cle = $_territoire[$champ];
					if (!isset($listes[$_critere][$cle])) {
						if ($champ_nom) {
							if (strpos($_critere, 'parent_') === false) {
								$listes[$_critere][$cle] = $_territoire[$champ_nom];
							} else {
								// On extrait la profondeur et on la compare à celle du territoire
								$profondeur = (int) str_replace('parent_', '', $_critere);
								if ($profondeur === (int) $_territoire['profondeur_type']) {
									$listes[$_critere][$cle] = $_territoire[$champ_nom];
								}
							}
						} else {
							$listes[$_critere][$cle] = territoire_carte_traduire_champ($champ, $_territoire[$champ]);
						}
					}
				}
			}

			// -- on transmet les listes au formulaire
			set_request('_filtres', $criteres_autorises);
			foreach ($filtres as $_critere => $_parametres) {
				set_request($_parametres['variable_liste'], $listes[$_critere]);
			}
		}
	}

	return $erreurs;
}

/**
 * Vérification de l'étape 2 du formulaire :
 *
 * @return array Message d'erreur si
 */
function formulaires_creer_territoire_carte_verifier_2() : array {
	// Initialisation des erreurs de vérification.
	$erreurs = [];

	if (_request('_etape') >= 2) {
		// On collecte donc les paramètres de l'étape 1.
		$type_carte = _request('type_carte');
		$type = _request('type');
		$variable_pays = 'pays_' . $type;
		$pays = _request($variable_pays) ?? '';

		if ($type_carte === 'carte') {
			$cartes = _request('cartes');
			set_request('_cartes', $cartes);
		} else {
			// On cherche la liste des territoires constitutifs de la carte en cours de construction
			// -- initialiser les filtres provenant de l'étape 1
			$filtres = ['type' => $type];
			if ($pays) {
				$filtres['iso_pays'] = $pays;
			}
			// -- collecter les paramètres de l'étape 2, sachant qu'un seul filtre peut être choisi.
			include_spip('inc/config');
			$config_filtres = lire_config('territoires_cartes/filtres', []);
			$explication_filtre = [];
			if (
				($critere = _request('filtre_carte'))
				and ($valeur = _request($config_filtres[$critere]['variable_choix']))
			) {
				$champ = $config_filtres[$critere]['champ_id'];
				$filtres[$champ] = is_array($valeur)
					? implode(',', $valeur)
					: $valeur;
				$type_explication = strpos($critere, 'parent_') === false
					? $critere
					: 'parent';
				$explication_filtre = [
					'critere' => _T('territoire_carte:champ_parametres_' . $type_explication . '_label'),
					'valeur'  => $filtres[$champ]
				];
			}
			// -- Lister les territoires pour les proposer à l'exclusion
			include_spip('inc/territoire');
			$territoires = territoire_repertorier($filtres);
			$territoires = array_column($territoires, 'nom_usage', 'iso_territoire');

			// Transmettre les variables au formulaire
			set_request('_explication_filtre', $explication_filtre);
			set_request('_territoires_crible', $territoires);
		}
	}

	return $erreurs;
}

/**
 * Exécution du formulaire : .
 *
 * @return array Tableau retourné par le formulaire contenant toujours un message de bonne exécution ou
 *               d'erreur. L'indicateur editable est toujours à vrai.
 */
function formulaires_creer_territoire_carte_traiter() : array {
	// Initialisation du retour de la fonction
	$retour = [];

	// Récupération des paramètres de base de la carte et début de consolidation des champs de l'objet carte
	$type_carte = _request('type_carte');
	$type = _request('type');
	$titre = _request('titre');
	$carte = [
		'titre'           => $titre,
		'type_carte'      => $type_carte,
		'type_territoire' => $type,
		'parametres'      => json_encode([]),
	];
	if ($type_carte === 'carte') {
		$cartes = _request('cartes');
		if ($cartes) {
			$carte['parametres'] = json_encode(['id_carte' => $cartes]);
		}
	} else {
		// Ajouter le pays à la carte si besoin
		$parametres = [];
		$variable_pays = 'pays_' . $type;
		$pays = _request($variable_pays) ?? '';
		if ($pays) {
			$carte['iso_pays'] = $pays;
		}

		// Collecter les paramètres supplémentaires éventuellement choisi.
		include_spip('inc/config');
		$config_filtres = lire_config('territoires_cartes/filtres', []);
		if (
			($critere = _request('filtre_carte'))
			and ($valeur = _request($config_filtres[$critere]['variable_choix']))
		) {
			$champ = $config_filtres[$critere]['champ_id'];
			$parametres[$champ] = $valeur;
		}

		// Collecter le mode et le crible final
		$mode_crible = _request('mode_crible');
		$crible = _request('crible') ?? [];
		// -- si inclusion et crible vide, on force le mode à exclusion pour avoir un contenu
		if (
			($mode_crible === 'inclusions')
			&& !$crible
		) {
			$mode_crible = 'exclusions';
		}
		$parametres[$mode_crible] = $crible;

		// On encode les paramètres en JSON
		$carte['parametres'] = json_encode($parametres);
	}

	// Insertion de la carte dans la table idoine.
	include_spip('action/editer_objet');
	$id_carte = objet_inserer('territoire_carte', null, $carte);
	if (!$id_carte) {
		$retour['message_erreur'] = _T('territoires_cartes:erreur_creation_carte');
	} else {
		// Redirection vers la page d'édition du taxon
		$retour['redirect'] = parametre_url(generer_url_ecrire('territoire_carte_edit'), 'id_carte', $id_carte);
	}

	return $retour;
}

/**
 * Renvoie la traduction de la valeur d'un champ `categorie` ou `profondeur_type` de la table des territoires.
 *
 * @param string     $champ  Nom du champ de la table des territoires.
 * @param int|string $valeur Valeur du champ pour le territoire concerné.
 *
 * @return string Traduction du champ pour le territoire concerné
 */
function territoire_carte_traduire_champ(string $champ, $valeur) : string {
	// Initialisation de la traduction
	$texte = '';

	if ($champ === 'categorie') {
		$texte = spip_ucfirst(_T('territoire:categorie_' . $valeur));
	} elseif ($champ === 'profondeur_type') {
		$texte = spip_ucfirst(_T('territoires_cartes:profondeur_' . (string) $valeur));
	}

	return $texte;
}
