<?php
/**
 * Gestion du formulaire de d'édition de territoire_carte.
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('inc/actions');
include_spip('inc/editer');

/**
 * Identifier le formulaire en faisant abstraction des paramètres qui ne représentent pas l'objet edité.
 *
 * @param int|string $id_carte    Identifiant du territoire_carte. 'new' pour un nouveau territoire_carte.
 * @param string     $retour      URL de redirection après le traitement
 * @param int        $lier_trad   Identifiant éventuel d'un territoire_carte source d'une traduction
 * @param string     $config_fonc Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array      $row         Valeurs de la ligne SQL de l'objet territoire_carte, si connu
 * @param string     $hidden      Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 *
 * @return string Hash du formulaire
 */
function formulaires_editer_territoire_carte_identifier_dist($id_carte = 'new', $retour = '', $lier_trad = 0, $config_fonc = '', $row = [], $hidden = '') {
	return serialize([(int) $id_carte]);
}

/**
 * Chargement du formulaire d'édition de territoire_carte.
 *
 * Déclarer les champs postés et y intégrer les valeurs par défaut
 *
 * @uses formulaires_editer_objet_charger()
 *
 * @param int|string $id_carte    Identifiant du territoire_carte. 'new' pour un nouveau territoire_carte.
 * @param string     $retour      URL de redirection après le traitement
 * @param int        $lier_trad   Identifiant éventuel d'un territoire_carte source d'une traduction
 * @param string     $config_fonc Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array      $row         Valeurs de la ligne SQL de l'objet territoire_carte, si connu
 * @param string     $hidden      Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 *
 * @return array Environnement du formulaire
 */
function formulaires_editer_territoire_carte_charger_dist($id_carte = 'new', $retour = '', $lier_trad = 0, $config_fonc = '', $row = [], $hidden = '') {
	// Charger l'objet Carte de territoires de façon standard
	$valeurs = formulaires_editer_objet_charger('territoire_carte', $id_carte, '', $lier_trad, $retour, $config_fonc, $row, $hidden);

	// Rajouter la liste des territoires et des exclusions si la carte est de type 'territoire'
	if (
		$valeurs
		and !empty($valeurs['type_carte'])
		and ($valeurs['type_carte'] === 'territoire')
	) {
		// -- liste des territoires sans crible
		include_spip('inc/territoire_carte');
		$territoires = territoire_carte_detourer($valeurs['id_carte'], 'iso_territoire', true);
		// -- Construire la liste de tous les territoires avec leur nom
		$territoires = sql_allfetsel('iso_territoire, nom_usage', 'spip_territoires', sql_in('iso_territoire', $territoires), '');
		$territoires = array_column($territoires, 'nom_usage', 'iso_territoire');
		asort($territoires);

		// -- liste des territoires inclus ou exclus si il y en a. Le mode est exclusif
		$parametres = json_decode($valeurs['parametres'], true);
		$mode_crible = isset($parametres['inclusions']) ? 'inclusions' : 'exclusions';
		$territoires_cribles = $parametres[$mode_crible] ?? [];

		$valeurs['mode_crible'] = $mode_crible;
		$valeurs['_territoires_crible'] = $territoires;
		$valeurs['_crible_defaut'] = $territoires_cribles;
	}

	return $valeurs;
}

/**
 * Vérifications du formulaire d'édition de territoire_carte.
 *
 * Vérifier les champs postés et signaler d'éventuelles erreurs
 *
 * @uses formulaires_editer_objet_verifier()
 *
 * @param int|string $id_carte    Identifiant du territoire_carte. 'new' pour un nouveau territoire_carte.
 * @param string     $retour      URL de redirection après le traitement
 * @param int        $lier_trad   Identifiant éventuel d'un territoire_carte source d'une traduction
 * @param string     $config_fonc Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array      $row         Valeurs de la ligne SQL de l'objet territoire_carte, si connu
 * @param string     $hidden      Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 *
 * @return array Tableau des erreurs
 */
function formulaires_editer_territoire_carte_verifier_dist($id_carte = 'new', $retour = '', $lier_trad = 0, $config_fonc = '', $row = [], $hidden = '') {
	return formulaires_editer_objet_verifier('territoire_carte', $id_carte);
}

/**
 * Traitement du formulaire d'édition de territoire_carte.
 *
 * Traiter les champs postés
 *
 * @uses formulaires_editer_objet_traiter()
 *
 * @param int|string $id_carte    Identifiant du territoire_carte. 'new' pour un nouveau territoire_carte.
 * @param string     $retour      URL de redirection après le traitement
 * @param int        $lier_trad   Identifiant éventuel d'un territoire_carte source d'une traduction
 * @param string     $config_fonc Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array      $row         Valeurs de la ligne SQL de l'objet territoire_carte, si connu
 * @param string     $hidden      Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 *
 * @return array Retours des traitements
 */
function formulaires_editer_territoire_carte_traiter_dist($id_carte = 'new', $retour = '', $lier_trad = 0, $config_fonc = '', $row = [], $hidden = '') {
	return formulaires_editer_objet_traiter('territoire_carte', $id_carte, '', $lier_trad, $retour, $config_fonc, $row, $hidden);
}
