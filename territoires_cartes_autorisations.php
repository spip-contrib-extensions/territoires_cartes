<?php
/**
 * Définit les autorisations du plugin Cartes.
 *
 * @plugin     Cartes
 *
 * @copyright  2021
 * @author     Eric Lupinacci
 *
 * @licence    GNU/GPL
 *
 * @package    SPIP\Cartes_territoires\Autorisations
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Fonction d'appel pour le pipeline.
 *
 * @pipeline autoriser */
function territoires_cartes_autoriser() {
}

// -----------------
// Objet cartes_territoires

/**
 * Autorisation de voir un l'élément de menu renvoyant vers la liste des cartes.
 *
 * @param string $faire Action demandée
 * @param string $type  Type d'objet sur lequel appliquer l'action
 * @param int    $id    Identifiant de l'objet
 * @param array  $qui   Description de l'auteur demandant l'autorisation
 * @param array  $opt   Options de cette autorisation
 *
 * @return bool true s'il a le droit, false sinon
**/
function autoriser_territoirecartes_menu_dist($faire, $type, $id, $qui, $opt) {
	return true;
}

/**
* Autorisation de voir la liste des cartes.
*
* @param  string $faire Action demandée
* @param  string $type  Type d'objet sur lequel appliquer l'action
* @param  int    $id    Identifiant de l'objet
* @param  array  $qui   Description de l'auteur demandant l'autorisation
* @param  array  $opt   Options de cette autorisation
*
* @return bool          true s'il a le droit, false sinon
**/
function autoriser_territoirecartes_voir_dist($faire, $type, $id, $qui, $opt) {
	return true;
}

/**
* Autorisation de voir une carte.
*
* @param  string $faire Action demandée
* @param  string $type  Type d'objet sur lequel appliquer l'action
* @param  int    $id    Identifiant de l'objet
* @param  array  $qui   Description de l'auteur demandant l'autorisation
* @param  array  $opt   Options de cette autorisation
*
* @return bool          true s'il a le droit, false sinon
**/
function autoriser_territoirecarte_voir_dist($faire, $type, $id, $qui, $opt) {
	return true;
}

/**
 * Autorisation de créer une carte.
 *
 * @param string $faire Action demandée
 * @param string $type  Type d'objet sur lequel appliquer l'action
 * @param int    $id    Identifiant de l'objet
 * @param array  $qui   Description de l'auteur demandant l'autorisation
 * @param array  $opt   Options de cette autorisation
 *
 * @return bool true s'il a le droit, false sinon
**/
function autoriser_territoirecarte_creer_dist($faire, $type, $id, $qui, $opt) {
	return in_array($qui['statut'], ['0minirezo', '1comite']);
}

/**
 * Autorisation de modifier une carte.
 *
 * @param string $faire Action demandée
 * @param string $type  Type d'objet sur lequel appliquer l'action
 * @param int    $id    Identifiant de l'objet
 * @param array  $qui   Description de l'auteur demandant l'autorisation
 * @param array  $opt   Options de cette autorisation
 *
 * @return bool true s'il a le droit, false sinon
**/
function autoriser_territoirecarte_modifier_dist($faire, $type, $id, $qui, $opt) {
	return in_array($qui['statut'], ['0minirezo', '1comite']);
}

/**
 * Autorisation de supprimer une carte
 *
 * @param string $faire Action demandée
 * @param string $type  Type d'objet sur lequel appliquer l'action
 * @param int    $id    Identifiant de l'objet
 * @param array  $qui   Description de l'auteur demandant l'autorisation
 * @param array  $opt   Options de cette autorisation
 *
 * @return bool true s'il a le droit, false sinon
**/
function autoriser_territoirecarte_supprimer_dist($faire, $type, $id, $qui, $opt) {
	$autoriser = false;

	include_spip('inc/territoire_carte');

	return $qui['statut'] == '0minirezo' and !$qui['restreint'];
}

/**
 * Autorisation de lier/délier l'élément.
 *
 * @param string $faire Action demandée
 * @param string $type  Type d'objet sur lequel appliquer l'action
 * @param int    $id    Identifiant de l'objet
 * @param array  $qui   Description de l'auteur demandant l'autorisation
 * @param array  $opt   Options de cette autorisation
 *
 * @return bool true s'il a le droit, false sinon
**/
function autoriser_associerterritoirecarte_dist($faire, $type, $id, $qui, $opt) {
	return $qui['statut'] == '0minirezo' and !$qui['restreint'];
}
