<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/territoires_cartes.git

return [

	// E
	'erreur_type_territoire_0_carte' => 'Aucune carte du type demandé n’est disponible.',
	'explication_etape_1' => 'Vous avez choisi de créer une carte définie par les paramètres suivants',
	'explication_etape_1_complement' => 'Vous pouvez ajouter un filtre supplémentaire',
	'explication_etape_2_complement' => 'Choisissez, soit d’inclure une partie des territoires ci-dessous, soit d’en exclure certains.
	<br>Pour inclure tous les territoires choisissez le mode `exclusion` en ne sélectionnant aucun territoire.
	<br>Si vous choissez le mode `inclusion` sans sélectionner des territoires, le mode `exclusion` sera forcé.',
	'explication_type_territoire' => 'Une carte est un ensemble de territoires du même type.',

	// L
	'label_carte_titre' => 'Titre de la carte',
	'label_cartes_disponibles' => 'Choisissez les cartes à inclure dans la carte à créer',
	'label_filtre_categorie' => 'Filtrer par catégorie',
	'label_filtre_parent' => 'Filtrer par parent',
	'label_filtre_parent_0' => 'Filtrer par racine du type du territoire',
	'label_filtre_parent_1' => 'Filtrer par enfant direct de la racine du type du territoire',
	'label_filtre_parent_2' => 'Filtrer par petit-enfant de la racine du type du territoire',
	'label_filtre_profondeur' => 'Filtrer par profondeur',
	'label_mode_crible_exclusion' => 'Exclure certains territoires',
	'label_mode_crible_inclusion' => 'Inclure manuellement les territoires',
	'label_parametre_categories' => 'Catégories autorisées',
	'label_parametre_parent' => 'Parents possibles',
	'label_parametre_profondeur' => 'Profondeurs dans le type',
	'label_pays_type' => 'Choisissez un pays',
	'label_type_carte' => 'Choisissez le mode de définition de la carte',
	'label_type_territoire' => 'Choisissez le type de territoires composant la carte',

	// M
	'menu_lister' => 'Les cartes',

	// P
	'profondeur_0' => 'Racine du type de territoire',
	'profondeur_1' => 'Enfant direct de la racine',
	'profondeur_2' => 'Petit-enfant de la racine',
	'profondeur_3' => 'Enfant de niveau 3',

	// T
	'territoire_cartes_titre' => 'Cartes de territoires',
	'titre_liste_enfant_carte' => 'Cartes composant la carte',
	'titre_liste_enfant_territoire' => 'Territoires composant la carte',
	'titre_page_creer' => 'Créer une carte',
	'titre_page_territoire_cartes' => 'Cartes de territoires',
];
