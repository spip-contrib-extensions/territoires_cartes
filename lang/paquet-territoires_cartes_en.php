<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/territoires_cartes-paquet-xml-territoires_cartes?lang_cible=en
// ** ne pas modifier le fichier **

return [

	// T
	'territoires_cartes_description' => 'This plugin lets you create maps composed of a set of territory objects. The maps thus created can then be displayed by aggregating the boundaries of each territory included.',
	'territoires_cartes_nom' => 'Territory Maps',
	'territoires_cartes_slogan' => 'Combining territories to build a map',
];
