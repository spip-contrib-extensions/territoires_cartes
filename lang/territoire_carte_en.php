<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/territoire_carte-territoires_cartes?lang_cible=en
// ** ne pas modifier le fichier **

return [

	// A
	'ajouter_lien_territoire_carte' => 'Add this map',

	// C
	'champ_contour_label' => 'Geographical display of the territories included in the map',
	'champ_descriptif_label' => 'Description',
	'champ_parametres_categorie_label' => 'Territory categories',
	'champ_parametres_exclusions_label' => 'Excluded territories',
	'champ_parametres_parent_label' => 'Parents of territories',
	'champ_parametres_pays_label' => 'Country of origin',
	'champ_parametres_profondeur_label' => 'Depth of territories within the type',
	'champ_titre_label' => 'Map title',
	'champ_type_carte_label' => 'Map type',
	'champ_type_territoire_label' => 'Territory type',
	'confirmer_supprimer_territoire_carte' => 'Can you confirm map removal?',

	// I
	'icone_creer_territoire_carte' => 'Create a map',
	'icone_modifier_territoire_carte' => 'Edit this map',
	'info_1_territoire_carte' => '1 map',
	'info_aucun_territoire_carte' => 'No map',
	'info_nb_territoire_cartes' => '@nb@ maps',
	'info_territoire_cartes_auteur' => 'The maps of this author',

	// R
	'retirer_lien_territoire_carte' => 'Remove this map',
	'retirer_tous_liens_territoire_cartes' => 'Remove all maps',

	// S
	'supprimer_territoire_carte' => 'Delete this map',

	// T
	'texte_ajouter_territoire_carte' => 'Add a map',
	'texte_changer_statut_territoire_carte' => 'This map is :',
	'texte_creer_associer_territoire_carte' => 'Create and associate a map',
	'texte_definir_comme_traduction_territoire_carte' => 'This map is a translation of map ID :',
	'titre_langue_territoire_carte' => 'Language of this map',
	'titre_logo_territoire_carte' => 'Map logo',
	'titre_objets_lies_territoire_carte' => 'Linked to this map',
	'titre_page_territoire_cartes' => 'The maps',
	'titre_territoire_carte' => 'Map',
	'titre_territoire_cartes' => 'Maps',
	'titre_territoire_cartes_rubrique' => 'Section maps',
	'type_carte_carte' => 'map list',
	'type_carte_territoire' => 'territory list',
];
