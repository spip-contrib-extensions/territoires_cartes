<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/territoires_cartes.git

return [

	// T
	'territoires_cartes_description' => 'Ce plugin permet de créer des cartes composées d’un ensemble d’objets territoire. Les cartes ainsi créées peuvent alors être affichées par agrégation des contours de chaque territoire inclus.',
	'territoires_cartes_nom' => 'Cartes de territoires',
	'territoires_cartes_slogan' => 'Regrouper des territoires pour former une carte',
];
