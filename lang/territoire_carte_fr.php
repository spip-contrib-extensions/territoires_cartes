<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/territoires_cartes.git

return [

	// A
	'ajouter_lien_territoire_carte' => 'Ajouter cette carte',

	// C
	'champ_contour_label' => 'Représentation géographique des territoires composant la carte',
	'champ_descriptif_label' => 'Descriptif',
	'champ_parametres_categorie_label' => 'Catégories des territoires',
	'champ_parametres_exclusions_label' => 'Territoires exclus',
	'champ_parametres_parent_label' => 'Parents directs des territoires',
	'champ_parametres_pays_label' => 'Pays d’appartenance des territoires',
	'champ_parametres_profondeur_label' => 'Profondeur des territoires dans le type',
	'champ_titre_label' => 'Titre de la carte',
	'champ_type_carte_label' => 'Type de carte',
	'champ_type_territoire_label' => 'Type de territoire',
	'confirmer_supprimer_territoire_carte' => 'Confirmez-vous la suppression de cette carte ?',

	// I
	'icone_creer_territoire_carte' => 'Créer une carte',
	'icone_modifier_territoire_carte' => 'Modifier cette carte',
	'info_1_territoire_carte' => 'Une carte',
	'info_aucun_territoire_carte' => 'Aucune carte',
	'info_nb_territoire_cartes' => '@nb@ cartes',
	'info_territoire_cartes_auteur' => 'Les cartes de cet auteur',

	// R
	'retirer_lien_territoire_carte' => 'Retirer cette carte',
	'retirer_tous_liens_territoire_cartes' => 'Retirer toutes les cartes',

	// S
	'supprimer_territoire_carte' => 'Supprimer cette carte',

	// T
	'texte_ajouter_territoire_carte' => 'Ajouter une carte',
	'texte_changer_statut_territoire_carte' => 'Cette carte est :',
	'texte_creer_associer_territoire_carte' => 'Créer et associer une carte',
	'texte_definir_comme_traduction_territoire_carte' => 'Cette carte est une traduction de la carte numéro :',
	'titre_langue_territoire_carte' => 'Langue de cette carte',
	'titre_logo_territoire_carte' => 'Logo de cette carte',
	'titre_objets_lies_territoire_carte' => 'Liés à cette carte',
	'titre_page_territoire_cartes' => 'Les cartes',
	'titre_territoire_carte' => 'Carte',
	'titre_territoire_cartes' => 'Cartes',
	'titre_territoire_cartes_rubrique' => 'Cartes de la rubrique',
	'type_carte_carte' => 'liste de cartes',
	'type_carte_territoire' => 'liste de territoires',
];
