<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/territoires_cartes-territoires_cartes?lang_cible=en
// ** ne pas modifier le fichier **

return [

	// E
	'erreur_type_territoire_0_carte' => 'No map of the requested type is available.',
	'explication_etape_1' => 'You have chosen to create a map defined by the following parameters',
	'explication_etape_1_complement' => 'You can add an extra filter',
	'explication_etape_2_complement' => 'You can still exclude territories', # MODIF
	'explication_type_territoire' => 'A map is a collection of territories of the same type.',

	// L
	'label_carte_titre' => 'Map title',
	'label_cartes_disponibles' => 'Choose the maps to combine in the map',
	'label_filtre_categorie' => 'Filter by category',
	'label_filtre_parent' => 'Filter by parent',
	'label_filtre_parent_0' => 'Filter by root of same territory type',
	'label_filtre_parent_1' => 'Filter by root child of same territory type ',
	'label_filtre_parent_2' => 'Filter by grandchild root of same territory type',
	'label_filtre_profondeur' => 'Filter by depth',
	'label_parametre_categories' => 'Allowed categories',
	'label_parametre_parent' => 'Possible parents',
	'label_parametre_profondeur' => 'Depths in type',
	'label_pays_type' => 'Select a country',
	'label_type_carte' => 'Select the map definition mode',
	'label_type_territoire' => 'Select the type of territory to be included on the map',

	// M
	'menu_lister' => 'Maps',

	// P
	'profondeur_0' => 'Territory type root',
	'profondeur_1' => 'Direct child of the root',
	'profondeur_2' => 'Grandchild of the root',
	'profondeur_3' => 'Level 3 child',

	// T
	'territoire_cartes_titre' => 'Territory maps',
	'titre_liste_enfant_carte' => 'Maps included in the map',
	'titre_liste_enfant_territoire' => 'Territories combined in the map',
	'titre_page_creer' => 'Create a map',
	'titre_page_territoire_cartes' => 'Territory maps',
];
