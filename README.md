# Plugin Cartes de Territoires

## Introduction

L’objectif du plugin est de créer des cartes composées de territoires du même type et de les afficher par agrégation de leurs contours.
Les cartes constituent un nouvel objet SPIP (`territoire_carte`).

Le plugin fournit une interface de type objet SPIP standard avec un assistant de création qui évite d’ajouter les territoires un par un.

## Documentation

La documentation utilisateur du plugin est disponible sur [SPIP-Contrib](https://contrib.spip.net/Cartes-de-territoires-utilisation-du-plugin).

## Installation

Le plugin s’installe comme n’importe quel plugin SPIP.
Il nécessite les plugins Territoires, Contours des Territoires et Rôles qui eux mêmes nécessitent d'autres plugins utiles à Contours des Territoires.

## L’objet carte de territoires

Une carte de territoires, comme son nom l’indique, est un ensemble cohérent de territoires du même type (au sens du plugin Territoires, à savoir, `zone`,
`country`, `subdivision`, etc.). Comme tout objet SPIP, une carte possède un identifiant numérique unique (id_carte) et un titre.

De façon à éviter de lier formellement les territoires à chaque carte par une table de liaison, ce qui engendrait de la redondance inutile, une carte est définie
par un ensemble de paramètres dont la conjonction permet de déterminer les territoires associés.

## La définition d’une carte

A cet égard, on distingue deux types de carte, les cartes composées d’autres cartes et, celles composées directement de territoires (on ne peut pas mixer les deux dans une seule carte).
Chacun de ces types nécessite une liste adéquate de paramètres.

Pour une carte composée d’autres cartes, seule la liste des identifiants des cartes incluses est nécessaire pour définir la liste exacte des territoires.
Toutes les cartes incluses sont forcément composées de territoires du même type. La profondeur d’imbrication des cartes n’est pas limitée.

Pour une carte composée directement de territoires, outre le type de territoire, les paramètres nécessaires sont les suivants :
* le pays si le type de territoires est une subdivisions, une infra-subdivisions ou une zone protégée (on ne regroupe pas ces types de territoires de plusieurs pays dans une même carte);
* un filtre, soit par catégorie (plusieurs possibles), soit par parenté, soit par profondeur dans le type (plusieurs possibles);
* une liste de territoires à exclure.

Un assistant de création permet de choisir ces paramètres et, pour les développeurs, une fonction d’API, `territoire_carte_detourer()`, permet de répertorier
la liste exacte des territoires de la carte, quelque soit son type et sa définition.

## L’interface utilisateur

L’interface d’utilisation fournit par le plugin Cartes de Territoires ne concerne que l’espace privé. Elle permet, au travers de plusieurs pages d’assurer les fonctions
de création et de consultation des cartes.

### Page liste des cartes

Cette page affiche, comme tous les objets SPIP, la liste des cartes de territoires. Aucun filtre n’est disponible actuellement.
Un bouton permet de lancer l’assistant de création d’une carte qui organise en 3 étapes le processus de saisie des paramètres de la carte.

### Page d’un objet carte

Cette page affiche la fiche objet de chaque carte. Comme tous les objets SPIP elle est composée d’une zone centrale dans laquelle est affichée le titre, le descriptif
de la carte et la représentation géographique des territoires composant la carte.
Dans le prolongement de la fiche centrale le plugin affiche la liste des territoires composant la carte.

Il est possible de supprimer la carte si celle-ci n’est pas utilisée dans une autre carte.

## Plugins connexes

Le plugin Carte de Territoires a pour but de constituer et d'afficher des cartes de tout type basées sur des territoires normalisés.
De telles cartes sont utilisées à des fins de présentation graphique de statistiques: elles permettent, par exemple, d'afficher des couleurs pour chaque
territoire en fonction de la valeur statistique affichée.

C'est le but du plugin Jeux de données pour Territoires de fournir ces données statistiques.
