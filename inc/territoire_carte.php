<?php
/**
 * Ce fichier contient l'API de gestion cartes de territoires.
 *
 * @package SPIP\TERRITOIRES_CARTES\CARTE\API
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Renvoie l'information ou les informations brutes demandées pour l'ensemble des cartes d'un type donné
 * ou toute la description si aucune information n'est explicitement demandée.
 * La liste peut être filtrée sur un ou plusieurs champs.
 *
 * @api
 *
 * @param string     $type_territoire Type de territoire. Les autres paramètres de la carte sont passés dans les filtres.
 * @param null|array $filtres         Liste des couples (champ, valeur) ou tableau vide. Ne pas utiliser le type de territoire.
 * @param null|array $informations    Identifiant d'un champ ou de plusieurs champs de la description d'un territoire.
 *                                    Si l'argument est vide, la fonction renvoie les descriptions complètes.
 *
 * @return array Description complète ou partielle des cartes.
 */
function territoire_carte_repertorier(string $type_territoire, ?array $filtres = [], ?array $informations = []) : array {
	// Utilisation d'une statique pour éviter les requêtes multiples sur le même hit.
	static $cartes = [];

	if (!isset($cartes[$type_territoire])) {
		// On récupère la description complète de toutes les cartes du type choisi
		$from = ['spip_territoires_cartes'];
		$where = ['type_territoire=' . sql_quote($type_territoire)];
		$order_by = ['id_carte'];
		$cartes[$type_territoire] = sql_allfetsel('*', $from, $where, '', $order_by);
	}

	// Refactoring du tableau suivant les champs demandés et application des filtres.
	$cartes_filtrees = [];
	$informations = $informations ? array_flip($informations) : [];
	foreach ($cartes[$type_territoire] as $_cle => $_carte) {
		// On détermine si on retient ou pas la carte.
		$filtre_ok = true;
		foreach ($filtres as $_champ => $_valeur) {
			if (isset($_carte[$_champ]) and ($_carte[$_champ] != $_valeur)) {
				$filtre_ok = false;
				break;
			}
		}

		// Ajout de la carte si le filtre est ok.
		if ($filtre_ok) {
			$cartes_filtrees[] = $informations
				? array_intersect_key($cartes[$type_territoire][$_cle], $informations)
				: $cartes[$type_territoire][$_cle];
		}
	}

	return $cartes_filtrees;
}

/**
 * Renvoie la description augmentée d'une carte de territoires (description de l'objet carte, listes des territoires,
 * ID des contours).
 *
 * @api
 *
 * @param int $id_carte Type de territoire. Les autres paramètres de la carte sont passés dans les filtres.
 *
 * @return array Description augmentée d'une carte.
 */
function territoire_carte_informer(int $id_carte) : array {
	// Utilisation d'une statique pour éviter les requêtes multiples sur le même hit.
	static $cartes = [];

	if (!isset($cartes[$id_carte])) {
		// On récupère la description complète de la carte
		include_spip('action/editer_objet');
		$carte = objet_lire('territoire_carte', $id_carte);
		if ($carte) {
			// Déterminer les territoires composant la carte
			$carte['territoires'] = territoire_carte_detourer($id_carte);

			// En extraire la liste des contours GIS associés
			$carte['contours'] = [];
			if (!empty($carte['territoires'])) {
				$where = [
					'objet=' . sql_quote('territoire'),
					sql_in('id_objet', $carte['territoires'])
				];
				$id_gis = sql_allfetsel('id_gis', 'spip_gis_liens', $where);
				$carte['contours'] = array_column($id_gis, 'id_gis');
			}
		}

		// Sauvegarde de la description
		$cartes[$id_carte] = $carte;
	}

	return $cartes[$id_carte];
}

/**
 * Renvoie la liste des territoires composant une carte.
 *
 * @api
 *
 * @param int         $id_carte    Identifiant unique de la carte.
 * @param null|string $champ_id    Nom du champ identifiant des territoires à retourner (id_territoire ou iso_territoire)
 * @param null|bool   $sans_crible Indique si la fonction doit retourner la liste des territoires avec crible ou pas
 *                                 (cad sans inclusion ou exclusion). Par défaut à `false`.
 *
 * @return array Liste des territoires avec ou sans crible.
 */
function territoire_carte_detourer(int $id_carte, ?string $champ_id = 'id_territoire', ?bool $sans_crible = false) : array {
	// On renvoie la liste des id de territoire
	$ids = [];

	// On récupère la description complète de la carte
	if (
		include_spip('action/editer_objet')
		and ($carte = objet_lire('territoire_carte', $id_carte))
	) {
		// On extrait les paramètres de la carte
		$parametres = json_decode($carte['parametres'], true);

		// On traite les deux types de cartes
		// -- territoire : on liste les territoires à partir des paramètres
		// -- carte : on fait une récursion sur les cartes incluses
		if ($carte['type_carte'] === 'territoire') {
			// On construit la sélection des territoires à partir des paramètres de la carte
			// -- le type de territoire est toujours un critère
			$where = [
				'type=' . sql_quote($carte['type_territoire'])
			];
			// -- les autres critères si ils existent :
			//    - pays,
			if (!empty($carte['iso_pays'])) {
				$where[] = 'iso_pays=' . sql_quote($carte['iso_pays']);
			}
			//    - et les paramètres parent, catégorie, profondeur et exclusions
			if (!empty($parametres)) {
				foreach ($parametres as $_parametre => $_valeurs) {
					if (
						(
							!$sans_crible
							or ($sans_crible && !in_array($_parametre, ['exclusions', 'inclusions']))
						)
						&& !empty($_valeurs)
					) {
						$champ = in_array($_parametre, ['exclusions', 'inclusions'])
							? 'iso_territoire'
							: $_parametre;
						$not = $_parametre === 'exclusions' ? 'NOT' : '';
						$where[] = sql_in($champ, $_valeurs, $not);
					}
				}
			}

			// Récupération des id de territoires
			$ids = sql_allfetsel($champ_id, 'spip_territoires', $where, '');
			$ids = array_column($ids, $champ_id);
		} elseif (!empty($parametres['id_carte'])) {
			foreach ($parametres['id_carte'] as $_id_carte) {
				$ids = array_merge($ids, territoire_carte_detourer($_id_carte, $champ_id));
			}
		}

		// On supprime les éventuels doublons
		$ids = array_unique($ids);
	}

	return $ids;
}
