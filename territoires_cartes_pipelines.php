<?php
/**
 * Utilisations des pipelines par Cartes de Territoires.
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Optimiser la base de données en supprimant les liens orphelins de l'objet vers quelqu'un et de quelqu'un vers l'objet.
 *
 * @pipeline optimiser_base_disparus
 *
 * @param array $flux Données du pipeline
 *
 * @return array Données du pipeline mises à jour
 */
function territoires_cartes_optimiser_base_disparus($flux) {
	include_spip('action/editer_liens');
	$flux['data'] += objet_optimiser_liens(['territoire_carte' => '*'], '*');

	return $flux;
}


/**
 * Enlever `id_carte` de la table spip_territoires du critère selections conditionnelles.
 *
 * @pipeline exclure_id_conditionnel
 *
 * @param array $flux Données du pipeline
 *
 * @return array Données du pipeline mises à jour
 */
function territoires_cartes_exclure_id_conditionnel(array $flux) : array {
	if (
		in_array(
			$flux['args']['table'],
			['spip_territoires']
		)
	) {
		$flux['data'] = array_merge($flux['data'], ['id_carte']);
	}
	return $flux;
}
