# Changelog

## [Unreleased]

## [1.0.5]

## Added

- Ajout du rechargement de la configuration par utilisation du nouveau pipeline configuration_recharger
- Ajout de la liste des cartes dans le menu global des Territoires
- Ajout d'un filtre sur le parent d'un même type quand ce type possède plusieurs profondeurs de territoires
- Ajout de la possibilité d'inclure des territoires un par un en fin de création d'une carte avec choix entre exclusion ou inclusion

## [1.0.4] - 2024-10-12

Avant d'installer cette version il convient absolument de désinstaller le plugin, ce qui provoquera la suppression de toutes
les cartes déjà créées qui seront perdues.

### Changed

- Actions qualité et PHPDoc
- Changement du préfixe du plugin (voir avertissement)

## [1.0.3] - 2024-07-01

### Changed

- Mise au point de l'icone de cartes
- Mise au point du formulaire
- Mise à jour des compatibilités de plugin

## [1.0.2] - 2023-12-17

Première version complète avec l'assistant de création de cartes opérationnel.
